#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi
path=$PWD
echo $path
sudo apt update -y && sudo snap install john-the-ripper
sudo apt install -y lzma liblzma-dev
sudo perl $path/Makefile.PL
sudo make
sudo make install
