#!/usr/bin/env python
import os

def check_input():
    while True:
        path = input("Input 7z ABSOLUTE path (ex. /home/[user]/Desktop/sample.7z) : ")
        if os.path.isfile(path) is False:
            print("Please check the path")
            continue
        while True:
            wordlist_path = input("Input wordlist path (ex. /home/[user]/Desktop/wordlist.txt)\nNo wordlist? Just Press ENTER : ")
            if os.path.isfile(wordlist_path) == True or wordlist_path == '':
                return path, wordlist_path
            else:
                print("Please check the path")


def john():
    path, wordlist_path = check_input()
    dir_path = os.path.dirname(os.path.realpath(__file__))
    perl_path = dir_path + "/7z2hashcat.pl"
    os.system("perl {} {} > {}/7z_hash.txt".format(perl_path, path, dir_path))
    john_cmd = "john {}/7z_hash.txt".format(dir_path)
    print(john_cmd)
    os.system(john_cmd)
    os.system("rm -rf ~/.john")


if __name__ == "__main__":
    john()