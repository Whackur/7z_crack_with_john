### How to install?
requirements : Python3.* +, git
Fully tested on Ubuntu & Kali Linux

```
git clone https://gitlab.com/whackur/7z_crack_with_john.git
cd 7z_crack_with_john/install/
tar zxvf Compress-Raw-Lzma-2.085.tar.gz --strip-components=1
sudo ./install.sh
```


### How to use?
Just run '7zCWJohn.py' as python3.x

```
python3.6 7zCWJohn.py
```

Cracked password is in john.pot file.
check the path

```
cd ~/snap/john-the-ripper/265/.john$

or...

root@kali:~# cd /root/.john/
root@kali:~/.john# 
```



### Reference from...
7z2hashcat

https://github.com/philsmd/7z2hashcat/blob/master/7z2hashcat.pl